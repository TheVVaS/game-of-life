GOL = {

    size: 50,

    cellBorderColor: '#DDDDDD',
    cellAliveColor: '#333333',
    cellDeadColor: '#FFFFFF',

    autoPlayInterval: null,
    autoPlaySpeed: 1,

    canvas: null,
    ctx: null,
    cellSize: [0,0],
    map: [],

    init: function () {

        // get canvas and it's context
        GOL.canvas = document.getElementById('play-screen');
        GOL.ctx = GOL.canvas.getContext('2d');

        // resize whole map
        GOL.onMapResize();

        // (re)start game
        GOL.firstTurn();

        // set listeners on wanted events
        GOL.addCanvasEventListeners();
        GOL.addMenuEventListeners();
    },

    //// EVENTS

    addCanvasEventListeners: function () {

        // add event listeners
        GOL.canvas.addEventListener('click', GOL.eventOnClick, false);
        document.addEventListener('keypress', GOL.eventOnKeyDown, false);
        window.addEventListener('resize', GOL.onMapResize, false);
    },

    addMenuEventListeners: function () {

        // button for next turn
        document.getElementById('play-next-turn').addEventListener('click', GOL.nextTurn, false);

        // refresh auto-play speed (on load and on change)
        window.addEventListener('load', GOL.updateAutoPlaySpeed, false);
        document.getElementById('auto-play-speed').addEventListener('input', GOL.updateAutoPlaySpeed, false);

        // auto-play start/stop
        document.getElementById('auto-play-start').addEventListener('click', GOL.startAutoPlay, false);
        document.getElementById('auto-play-stop').addEventListener('click', GOL.stopAutoPlay, false);

        // refresh map size (on load and on change)
        window.addEventListener('load', GOL.updateMapSize, false);
        document.getElementById('configuration-size').addEventListener('input', GOL.updateMapSize, false);

        // restart game
        document.getElementById('configuration-restart').addEventListener('click', GOL.firstTurn, false);

        // color picker
        document.getElementById('menu-colorpicker-border').addEventListener('input', GOL.updateColorPicker, false);
        document.getElementById('menu-colorpicker-alivecell').addEventListener('input', GOL.updateColorPicker, false);
        document.getElementById('menu-colorpicker-deadcell').addEventListener('input', GOL.updateColorPicker, false);
        window.addEventListener('load', GOL.startColorPicker, false);
    },

    eventOnClick: function (event) {

        // if canvas was scaled (to determine correct position)
        var scaleH = GOL.canvas.clientHeight / GOL.canvas.height;
        var scaleW = GOL.canvas.clientWidth / GOL.canvas.height;

        // find what cell was clicked
        var xPos = (event.pageX - GOL.canvas.offsetLeft) / scaleW;
        var yPos = (event.pageY - GOL.canvas.offsetTop) / scaleH;
        var xClicked = Math.floor(xPos / GOL.cellSize[0]);
        var yClicked = Math.floor(yPos / GOL.cellSize[1]);

        // clicked out of map
        if (typeof GOL.map[xClicked] === 'undefined') return;
        if (typeof GOL.map[xClicked][yClicked] === 'undefined') return;

        // change cell state
        GOL.setCellState([xClicked,yClicked], !GOL.getCellState([xClicked,yClicked]));
        GOL.redrawCells();
    },

    eventOnKeyDown: function (event) {

        // capture only space key
        if (event.code === 'Space') return;

        // next turn on key space pressed
        GOL.nextTurn();
    },

    //// MAP

    firstTurn: function () {

        // clear map
        GOL.map = [];

        // recalculate size of cells
        GOL.cellSize = [
            GOL.canvas.width / GOL.size,
            GOL.canvas.height / GOL.size
        ];

        // create all cells
        for (var x = 0; x < GOL.size; x++) {

            // prepare row for cell states
            GOL.map[x] = [];

            // starting state of all cells is dead
            for (var y = 0; y < GOL.size; y++) {
                GOL.setCellState([x,y], false);
            }
        }

        // auto-play should be stop by default on new start
        GOL.stopAutoPlay();

        // draw all cells
        GOL.redrawCells();
    },

    nextTurn: function () {

        // prepare states of cells in next turn
        GOL.map = GOL.getCellsNextState();

        // redraw all cells
        GOL.redrawCells();
    },

    onMapResize: function () {

        // width and height container of canvas
        var parentHeight = GOL.canvas.parentElement.clientHeight - 20;
        var parentWidth = GOL.canvas.parentElement.clientWidth - 20;

        // update map display size (let it be always square)
        GOL.canvas.style['width'] = ((parentWidth > parentHeight) ? parentHeight : parentWidth) - 2;
        GOL.canvas.style['height'] = ((parentWidth > parentHeight) ? parentHeight : parentWidth) - 2;
    },

    //// CELLS

    getCellsNextState: function () {

        // lvd
        var nextStates = [];

        for (var x = 0; x < GOL.size; x++) {

            // create
            nextStates[x] = [];

            for (var y = 0; y < GOL.size; y++) {
                nextStates[x][y] = GOL.getCellNextState([x,y]);
            }
        }

        return nextStates;
    },

    getCellNextState: function (position) {

        // lvd
        var neighboursCount = GOL.countAliveNeighbours(position);
        var cellState = GOL.getCellState(position);
        var nextState = cellState;

        // depending on current state and count of neighbours determine next state of cell
        if (cellState === false && neighboursCount === 3) {

            // cell is dead, but in next turn will be alive
            nextState = true;

        } else if (cellState === true && [2,3].indexOf(neighboursCount) === -1) {

            // cell is alive, but in next turn will be dead
            nextState = false;

        }

        return nextState;
    },

    countAliveNeighbours: function (position) {

        // lvd
        var neighboursCount = 0;
        var neighboursPositions = [
            [-1, -1], [ 0, -1], [ 1, -1],
            [-1,  0],           [ 1,  0],
            [-1,  1], [ 0,  1], [ 1,  1]
        ];

        // count all alive neighbours
        neighboursPositions.forEach(function (value) {

            // lvd
            var x = position[0] + value[0];
            var y = position[1] + value[1];

            // dont check these out of map
            if (x < 0 || x > GOL.size -1) return;
            if (y < 0 || y > GOL.size -1) return;

            // count if alive
            if (GOL.getCellState([x,y])) neighboursCount++;
        });

        return neighboursCount;
    },

    setCellState: function (position, state) {
        GOL.map[position[0]][position[1]] = state;
    },

    getCellState: function (position) {
        return GOL.map[position[0]][position[1]];
    },

    //// DRAWING

    redrawCells: function () {

        // clear canvas
        GOL.clearPlayScreen();

        // redraw all cells
        for (var x = 0; x < GOL.size; x++) {
            for (var y = 0; y < GOL.size; y++) {
                GOL.redrawCell([x,y]);
            }
        }
    },

    redrawCell: function (position) {

        // calculate XY position of cell
        var xPos = position[0] * GOL.cellSize[0];
        var yPos = position[1] * GOL.cellSize[1];

        // draw cell
        GOL.ctx.fillStyle = GOL.getCellState([position[0],position[1]]) ? GOL.cellAliveColor : GOL.cellDeadColor;
        GOL.ctx.fillRect(xPos, yPos, GOL.cellSize[0], GOL.cellSize[1]);

        // draw cell's border
        GOL.ctx.strokeStyle = GOL.cellBorderColor;
        GOL.ctx.strokeRect(xPos, yPos, GOL.cellSize[0], GOL.cellSize[1]);
    },

    clearPlayScreen: function () {
        GOL.ctx.clearRect(0, 0, GOL.canvas.width, GOL.canvas.height);
    },

    //// MENU

    updateAutoPlaySpeed: function () {

        // get auto-play speed
        GOL.autoPlaySpeed = document.getElementById('auto-play-speed').value;

        // update speed label
        document.getElementById('auto-play-speed-label').innerText = 'Turns Per Second: ' + GOL.autoPlaySpeed;

        // if interval is active update its speed
        if (GOL.autoPlayInterval !== null) GOL.startAutoPlay();
    },

    startAutoPlay: function () {

        // stop previous auto-play if exists
        GOL.stopAutoPlay();

        // mark auto-play as active
        document.getElementById('auto-play-start').style['color'] = 'red';
        document.getElementById('auto-play-stop').style['color'] = '';

        // set interval on nextTurn
        GOL.autoPlayInterval = setInterval(GOL.nextTurn, 1000 / GOL.autoPlaySpeed);
    },

    stopAutoPlay: function () {

        // stop interval
        clearTimeout(GOL.autoPlayInterval);

        // mark auto-play as inactive
        document.getElementById('auto-play-start').style['color'] = '';
        document.getElementById('auto-play-stop').style['color'] = 'red';

        // unset interval
        GOL.autoPlayInterval = null;
    },

    updateMapSize: function () {

        // get elements
        GOL.size = document.getElementById('configuration-size').value;

        // update size label
        document.getElementById('configuration-size-label').innerText = 'Map size: ' + GOL.size;

        // redraw map
        GOL.firstTurn();
    },

    startColorPicker: function () {

        // update visually all colorpickers on start
        ['menu-colorpicker-border', 'menu-colorpicker-alivecell', 'menu-colorpicker-deadcell'].forEach(function (elementId) {

            // lvd
            var defaultColor = null;
            var srcElement = document.getElementById(elementId);

            // get default color for each element
            switch (elementId)
            {
                case 'menu-colorpicker-border':    defaultColor = GOL.cellBorderColor; break;
                case 'menu-colorpicker-alivecell': defaultColor = GOL.cellAliveColor;  break;
                case 'menu-colorpicker-deadcell':  defaultColor = GOL.cellDeadColor;   break;
            }

            // set color preview on colorpicker element
            srcElement.style['border-bottom'] = '5px solid ' + defaultColor;

            // set value of element
            srcElement.value = defaultColor;
        });
    },

    updateColorPicker: function (event) {

        // get changed element
        var srcElement = event.srcElement;

        // correct value given by user
        if (srcElement.value[0] !== '#') srcElement.value = '#' + srcElement.value;
        srcElement.value = srcElement.value.toUpperCase();
        srcElement.value = srcElement.value.replace(/[^0-9A-F#]/i, '');

        // set color preview on colorpicker element
        srcElement.style['border-bottom'] = '2px solid ' + srcElement.value;

        // depending on what input was changed update color scheme
        switch (srcElement.id)
        {
            case 'menu-colorpicker-border':    GOL.cellBorderColor = srcElement.value; break;
            case 'menu-colorpicker-alivecell': GOL.cellAliveColor = srcElement.value;  break;
            case 'menu-colorpicker-deadcell':  GOL.cellDeadColor = srcElement.value;   break;
        }

        // redraw everything with new color scheme
        GOL.redrawCells();
    }
};

GOL.init();